import unittest

from snowflake_ci import script_executor


class TestScriptExecutor(unittest.TestCase):
    """
    Test Script Executor Class
    """

    def test_process_scripts(self):
        self.assertTrue(script_executor.process_scripts(15))

    def test_process_scripts_false(self):
        self.assertFalse(script_executor.process_scripts(5))

    def test_process_scripts_equal(self):
        self.assertFalse(script_executor.process_scripts(10))


if __name__ == '__main__':
    unittest.main()
