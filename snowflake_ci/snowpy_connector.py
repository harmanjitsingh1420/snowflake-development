#!/usr/bin/env python
import snowflake.connector
import os
from snowflake_ci.read_scripts import ReadScripts
from dotenv import load_dotenv


class SF_Connector:
    def __init__(self):
        load_dotenv()

        print("Initializing connection...")
        self.__accountId__ = os.getenv("snow_account_id")
        self.__username__ = os.getenv("snow_username")
        self.__password__ = os.getenv("snow_user_password")

        print(f"Connection configured for user: {os.getenv('snow_username')}")

    def get_connection(self):
        print("Connecting to snowflake...")
        return snowflake.connector.connect(
            user=self.__username__,
            password=self.__password__,
            account=self.__accountId__
        )

    def execute_scripts(self, cs):
        print("Executing sql_scripts...")
        readScript = ReadScripts()
        scripts = readScript.read_all_files()
        for script in scripts:
            try:
                cs.execute(script)
                print(f"procedure {script} executed successfully")
            except snowflake.connector.errors.ProgrammingError as e:
                print(e)
                print('Error {0} ({1}): {2} ({3})'.format(e.errno, e.sqlstate, e.msg, e.sfqid))
        cs.close()