import json


def load_config(configPath):
    with open(configPath, "r") as f:
        configs = json.load(f)
    return configs
