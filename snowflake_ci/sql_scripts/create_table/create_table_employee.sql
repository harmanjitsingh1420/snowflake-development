DROP TABLE DEMO_DB.PUBLIC.Employees2;

CREATE TABLE DEMO_DB.PUBLIC.Employees2(
emp_id NUMBER(5,0),
emp_name STRING,
salary NUMBER(10,2),
phone_number NUMBER(10,0)
);
