DROP TABLE DEMO_DB.PUBLIC.Student;

CREATE TABLE DEMO_DB.PUBLIC.Student(
stud_id NUMBER(5,0),
stud_name STRING,
class STRING,
marks NUMBER(4,2)
);

