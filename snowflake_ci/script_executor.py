from snowflake_ci.snowpy_connector import SF_Connector


def main():
    sfc = SF_Connector()
    ctx = sfc.get_connection()
    print("Connection successful")

    sfc.execute_scripts(ctx.cursor())
    ctx.close()


def process_scripts(val):
    if val > 10:
        return True
    else:
        return False


if __name__ == '__main__':
    main()
