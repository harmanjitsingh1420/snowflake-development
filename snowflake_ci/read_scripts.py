import os
from snowflake_ci.utils import LoadJsonConfigurations


class ReadScripts:
    def __init__(self):
        conf = LoadJsonConfigurations.load_config("snowflake_ci/config/scriptConfig.json")
        os.chdir(os.path.join(os.getcwd(), conf["script_path"]))

    def read_text_file(self, scriptFile):
        with open(scriptFile, 'r') as scf:
            statement_list = scf.read().split(";")
            formatted_list = []
            for statement in statement_list:
                formatted_list.append(statement.strip())
            return formatted_list

    def read_all_files(self):
        scripts = []
        for r, d, f in os.walk(os.getcwd()):
            for file in f:
                if file.endswith(".sql"):
                    files=self.read_text_file(os.path.join(r, file))
                    for sc in files:
                        scripts.append(sc)

        # for file in os.listdir():
        #     if file.endswith(".sql"):
        #         file_path = os.path.join(os.getcwd(), file)
        #         scripts.append(self.read_text_file(file_path))
        return scripts
