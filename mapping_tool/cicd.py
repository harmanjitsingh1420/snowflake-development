import os.path
import glob
import pandas as pd
import yaml
import shutil


class Xl2SqlScriptBuild:

    def __init__(self, **kwargs):
        self._df = pd.DataFrame()
        self._pipe = kwargs.get("file_to_process")

    def build_all(self) -> True:
        """
        Create sql cmd for create table.

        Returns:
            True if code is success, otherwise exception
        """

        ydata = Xl2SqlScriptBuild.get_files_to_process(self._pipe)
        # BLOB TO RAW
        for val in ydata['input']:
            _env = val['environment']
            _dir = val['dir']
            if val.get("files"):
                files = val.get("files")
            else:
                files = glob.glob(_dir + "/*")
            print(files, ">>>>>>>>>")
            # blob_to_raw
            blob_to_raw_dir = 'mapping_tool/blob_to_raw'
            print("Clearing blob_to_raw directory")
            print("Environment ==>", _env)
            shutil.rmtree(blob_to_raw_dir)
            # if os.path.exists(blob_to_raw_dir):
            #     os.rmdir(blob_to_raw_dir)
            os.mkdir(blob_to_raw_dir)

            for _file in files:
                path = os.path.join(_dir, _file)
                self._df = pd.read_excel(path, 'Raw_Mapping')

                self._df['RAW_DATABASE'] = self._df['RAW_DATABASE'].str.upper().str.replace("{ENVIRONMENT}", _env)
                self._df['RAW_SCHEMA'] = self._df['RAW_SCHEMA'].str.upper()
                self._df['RAW_TABLE_NAME'] = self._df['RAW_TABLE_NAME'].str.upper()
                self._df['RAW_CONVERSION_EXPRESSION'] = self._df['RAW_CONVERSION_EXPRESSION'].replace(
                    {'\'': 'sINGLEqUOTE'}, regex=True)
                self._df['RAW_QUALITY_VALIDATION'] = self._df['RAW_QUALITY_VALIDATION'].replace(
                    {'\'': 'sINGLEqUOTE'}, regex=True)
                self._df['COMMENTS'] = self._df['COMMENTS'].replace(
                    {'\'': 'sINGLEqUOTE'}, regex=True)

                fqtn = pd.DataFrame()
                fqtn['TABLE'] = self._df['RAW_DATABASE'] + "|" + self._df['RAW_SCHEMA'] + "|" \
                                + self._df['RAW_TABLE_NAME'] + "|" + self._df['RAW_TABLE_ID'].astype(str)

                # Iterate for unique db-schema-table combination
                for table in fqtn['TABLE'].unique():
                    db = (table.split('|')[0])
                    schema = (table.split('|')[1])
                    table_id = (table.split('|')[3])
                    table = (table.split('|')[2])

                    print('Processing Database :', db, ' Schema :', schema, ' Table :', table)
                    table_df = self._df[(self._df.RAW_DATABASE == db) & (self._df.RAW_SCHEMA == schema)
                                        & (self._df.RAW_TABLE_NAME == table)]

                    db_dir = os.path.join(blob_to_raw_dir, db)
                    if not os.path.exists(db_dir):
                        os.mkdir(db_dir)

                    schema_dir = os.path.join(db_dir, schema)
                    if not os.path.exists(schema_dir):
                        os.mkdir(schema_dir)

                    # --------------------------------------------------
                    # Create Mapping Insert into SQL
                    # --------------------------------------------------
                    #  create directory
                    mappings_dir = os.path.join(schema_dir, 'mappings')
                    mapping_template = os.path.join(_dir, 'blob_to_raw_mapping.txt')
                    mapping_file = os.path.join(mappings_dir, "post_0" + table_id + "_v0_COMMON_MAPPING_"
                                                + schema + "_" + table + ".sql")
                    if not os.path.exists(mappings_dir):
                        os.mkdir(mappings_dir)

                    #  read template and replace substitution string in template
                    with open(mapping_template, "r") as myfile:
                        data = myfile.readlines()
                    data1 = "".join(data).replace('MAPPING_DATABASE_NAME', db) \
                        .replace('MAPPING_SCHEMA_NAME', schema) \
                        .replace('MAPPING_TABLE_NAME', table)

                    _row_tup = list(table_df.fillna('NULL').applymap(str).to_records(index=False))
                    values = [str(_) for _ in _row_tup]
                    mapping_sql = data1.replace('MAPPING_INSERT_VALUES', ",".join(values)) \
                        .replace("'True'", "True") \
                        .replace("'False'", "False") \
                        .replace("'NULL'", "NULL") \
                        .replace("sINGLEqUOTE", "''").replace(",('", ",\n('")
                    with open(mapping_file, "w") as f1:
                        f1.write(mapping_sql)

                    # --------------------------------------------------
                    # Create Sequence SQL
                    # --------------------------------------------------
                    # create directory
                    sequence_dir = os.path.join(schema_dir, 'sequences')
                    sequence_template = os.path.join(_dir, 'blob_to_raw_sequence.txt')
                    sequence_file = os.path.join(sequence_dir, "SEQ_" + table + ".sql")
                    if not os.path.exists(sequence_dir):
                        os.mkdir(sequence_dir)

                    #  read template and replace substitution string in template
                    with open(sequence_template, "r") as myfile:
                        data = myfile.readlines()
                    sequence_sql = "".join(data).replace('MAPPING_DATABASE_NAME', db) \
                        .replace('MAPPING_SCHEMA_NAME', schema) \
                        .replace('MAPPING_TABLE_NAME', table)
                    with open(sequence_file, "w") as f1:
                        f1.write(sequence_sql)

                    # --------------------------------------------------
                    # Create Table SQL
                    # --------------------------------------------------
                    #  create directory
                    tables_dir = os.path.join(schema_dir, 'tables')
                    table_template = os.path.join(_dir, 'blob_to_raw_table.txt')
                    table_file = os.path.join(tables_dir, table + ".sql")
                    if not os.path.exists(tables_dir):
                        os.mkdir(tables_dir)

                    #  read template and replace substitution string in template
                    with open(table_template, "r") as myfile:
                        data = myfile.readlines()
                    column_format_str = ",\n        ".join(table_df["RAW_COLUMN_NAME"]
                                                           + ' ' + table_df["RAW_DATA_TYPE"]
                                                           + table_df["RAW_NULLABLE"].astype(str)
                                                           .replace('False', ' NOT NULL')
                                                           .replace('True', ' '))
                    primary_key_df = table_df[["RAW_COLUMN_NAME", "RAW_PK_ORDER"]] \
                        .dropna().sort_values(by='RAW_PK_ORDER')
                    primary_key_str = ", ".join(primary_key_df["RAW_COLUMN_NAME"])

                    table_sql = "".join(data).replace('MAPPING_DATABASE_NAME', db) \
                        .replace('MAPPING_SCHEMA_NAME', schema) \
                        .replace('MAPPING_TABLE_NAME', table) \
                        .replace('TABLE_PRIMARY_COLUMNS', primary_key_str) \
                        .replace('TABLE_COLUMN_DEFINITIONS', column_format_str) \
                        .replace(', CONSTRAINT PK_' + table + ' PRIMARY KEY ()', '')
                    with open(table_file, "w") as f1:
                        f1.write(table_sql)

                output_dir = os.path.join(os.getcwd(), "snowflake_ci/sql_scripts")

                for database in self._df['RAW_DATABASE'].unique():
                    files = glob.glob(os.path.join(blob_to_raw_dir, database) + '/**/*.sql',
                                      recursive=True)
                    database_script = os.path.join(output_dir, database + "/"
                                                   + database + "_script.sql")
                    shutil.rmtree(os.path.join(output_dir, database))
                    if not os.path.exists(os.path.join(output_dir, database)):
                        os.mkdir(os.path.join(output_dir, database))

                    with open(database_script, 'w') as outfile:
                        for fname in files:
                            with open(fname) as infile:
                                outfile.write(infile.read())

        return True

    @staticmethod
    def get_files_to_process(file_to_process: str) -> dict:
        """
        This is to read the yaml file, where input are mentioned
        Args:
            file_to_process: yaml file name or full path with name

        Returns:

        """

        with open(file_to_process, "r") as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
        return data


if __name__ == "__main__":

    file_path = os.path.join(os.getcwd(), "mapping_tool/files_to_process.yml")

    conf = dict(
        file_to_process=file_path
    )
    obj_xl2sql = Xl2SqlScriptBuild(**conf)

    if obj_xl2sql.build_all():
        print("It's Success")
